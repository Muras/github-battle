// In webpack.config.js
//Variables for HTML Webpack plugin
var HtmlWebpackPlugin = require('html-webpack-plugin')
var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
	template: __dirname + '/app/index.html',
	filename: 'index.html',
	inject: 'body'
});

//Webpack modules
module.exports = {
	entry: ['./app/index.js'], //wp entry file
	output: { //wp output path and filename
		filename: 'index_budle.js',
		path: __dirname + '/dist'
	},module: { //all used modules, here just coffee script
		loaders: [
			{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?presets[]=es2015&presets[]=react'
      },
		]
	},
	plugins: [HTMLWebpackPluginConfig]
};

