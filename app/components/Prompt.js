import React from 'react'
var PropTypes = React.PropTypes;

import styles from '../styles/styles'

//stateless function. We can use if we do not have states inside of it
function Prompt(props) {
  return (
    <div className='jumbotron col-sm-6 col-sm-offset-3 text-container'
      style={styles.playerPrompt}>
      <h1 style={styles.centerText}>{props.header}</h1>
      <div className='col-sm-22'>
        <form onSubmit={props.onSubmitUser}>
          <div className='form-group'>
            <input
              className='form-control'
              placeholder='Github Username'
              onChange={props.onUpdateUser}
              value={props.username}
              type='text' />
          </div>
          <div className='form-group col-sm-4 col-sm-offset-4'>
            <button
              className='btn btn-block btn-success'
              type='submit'>
              Continue
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

Prompt.propTypes = {
  onSubmitUser: PropTypes.func.isRequired,
  onUpdateUser: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired
}

export default Prompt