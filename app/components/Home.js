import React from 'react'
import styles from '../styles/styles'
import { ReactRouter, Link } from 'react-router'

export default React.createClass({
	render: function () {
		return (
			<div className='jumbotron col-sm-8 col-sm-offset-2 text-center' style={styles.playerPrompt}>
				<h1>Github Battle</h1>
				<p className='lead'>App made with ReactJS + Bootstrap</p>
				<Link to='/playerOne'>
					<button type='button' className='btn btn-lg btn-success'>
						Get started!
					</button>
				</Link>
			</div>
		)
	}
});
