var FriendsContainer = React.createClass({
	render: function () {
		var name = 'Tyler McGinnis';
		var friends = ['Ean Platter', 'Murphy Randall', 'Merrick Christensen'];
		return (
				<div>
					<h3>Name: {name}</h3>
					<ShowList names={friends}/>
				</div>
		)
	}
});

var ShowList = React.createClass({
	render: function () {
		var listItems = this.props.names.map(function (friend) {
			return (<li key={friend.id}>{friend}</li>);
		});
		return (
				<div>
					<h3>Friends list:</h3>
					{listItems}
				</div>
		)
	}
});
ReactDOM.render(<FriendsContainer />, document.getElementById('app'));
//=========================================================================
var USER_DATA = {
	name: 'Michal Murawski',
	username: 'Draer',
	image: 'https://media.licdn.com/media/AAEAAQAAAAAAAAjQAAAAJDQwMzEzYjAzLTBkNTMtNGZjZi05YzgyLWEwMDc4YjZkYzU5ZA.jpg'
};



var ProfilePic = React.createClass({
	render: function () {
		return <img src={this.props.imageUrl} style={{height: 100, width: 100}}/>
	}
});

var Link = React.createClass({
	render: function (){
		return (
				<span>
					"herere"
				</span>
		)
	}
})

var ProfileLink = React.createClass({
	render: function () {
		return (
				<div>
					<Link href={'http://www.github.com/' + this.props.username}>
						{this.props.username}
					</Link>
				</div>
		)
	}
});

var ProfileName = React.createClass({
	render: function () {
		return <div>{this.props.name}</div>;
	}
});

var Avatar = React.createClass({
	render: function () {
		return (
				<div>
					<ProfilePic imageUrl={this.props.user.image}/>
					<ProfileName name={this.props.user.name}/>
					<ProfileLink username={this.props.user.username}/>
				</div>
		)
	}
});

ReactDOM.render(
		<Avatar user={USER_DATA}/>,
		document.getElementById('app')
);