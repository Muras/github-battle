import React from 'react'
const PropTypes = React.PropTypes

import styles from '../styles/styles'

function puke(obj) {
  return <pre>{JSON.stringify(obj, 2, ' ') }</pre>
}

function Results(props) {
  return (
    <div className ='jumbotron col-sm-12 text-center' style ={styles.playerPrompt}>
      <h1>Results</h1>
      <div className='col-sm-8 col-sm-offset-2'>
      
      </div>
    </div>
  )
}

Results.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  scores: PropTypes.array.isRequired,
  playersInfo: PropTypes.array.isRequired
}

export default Results