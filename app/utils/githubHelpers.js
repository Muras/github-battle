import axios from 'axios'

const id = '0aafc717efa3d7f7d0a0'
const sec = 'e3df1830e01631805451a8b134a07b717bee166b'
const param = '?client_id=$' + id + '&client+_secret=' + sec

function getUserInfo(username) {
  return axios.get('https://api.github.com/users/' + username + param)
}

function getRepos(username) {
  return axios.get('https://api.github.com/users/' + username + '/repos' + param + '&per_page=100')
}

function getTotalStars(repos) {
  return repos.data.reduce(function (prev, current) {
    return prev + current.stargazers_count
  }, 0)
}

function getPlayersData(player) {
  return getRepos(player.login)
    .then(getTotalStars)
    .then(function (totalStars) {
      return {
        followers: player.followers,
        totalStars: totalStars
      }
    })
}

function calculateScores(players) {
  return [
    players[0].followers * 3 + players[0].totalStars,
    players[1].followers * 3 + players[1].totalStars
  ]
}

const helpers = {
  getPlayersInfo(players) {
    return axios.all(players.map(function (username) {
      return getUserInfo(username)
    })).then((info) => {
      return info.map((user) => {
        return user.data
      })
    }).catch((err) => {
      console.warn('Error in getPlayersInfo', err)
    })
  },
  battle(players) {
    let playerOneData = getPlayersData(players[0])
    let playerTwoData = getPlayersData(players[1])

    return axios.all([playerOneData, playerTwoData])
      .then(calculateScores)
      .catch(function (err) { console.log('Error in getPlayersInfo', err) })
  }

}

export default helpers