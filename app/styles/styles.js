const styles = {
  playerPrompt: {
    background: 'transparent'
  },
  centerText: {
    textAlign: 'center'
  },
  space: {
    marginTop: '20px'
  }
}

export default styles